const mysql = require('mysql');
const db_config = {
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'example'
};
const MAX_ATTEMPTS = 5;

function fnQuery(query) {
  let attempt_numbers = 0;

  let promise = (resolve, reject) => {
    let connection = mysql.createConnection(db_config);

    connection.connect(err => {
      if (err) {
        if (attempt_numbers < MAX_ATTEMPTS) {
          attempt_numbers++;
          setTimeout(() => {
            promise(resolve, reject);
          }, 2000);
          return;
        } else {
          reject(err);
        }
      }

      connection.query(query, (error, result, fields) => {
        if (error) {
          reject(error);
        }
        resolve(result);
      });

      connection.end();
    });

    connection.on('error', err => {
      if (err.code === 'PROTOCOL_CONNECTION_LOST') {
        promise(resolve, reject);
      } else {
        reject(err);
      }
    });

  };

  return new Promise(promise);

}

fnQuery('select * from letter')
  .then(result => {
    console.log(result);
  })
  .catch(err => {
    console.log(err);
  });
